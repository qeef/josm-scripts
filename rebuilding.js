/*
    Rebuilding - simplify and orthogonalize selected buildings.
    Copyright (C) 2018  Jiri Hubacek
    Contact: Jiri Hubacek <jiri.hubacek@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var MORE_TOOLS_INDEX = 4; // Index of "More tools" menu, start from 0.
var SIMPLIFY_AREA_INDEX = 0; // Index of "Simplify area" in "More tools" menu.

// In my case, MORE_TOOLS_INDEX is 4. My menu looks like
// "File", "Edit", "View", "Tools", "More Tools", "Selection", ...
// Because indexing from zero, it's 0, 1, 2, 3, 4 - "More Tools".
//
// Again for menu items, index starts form 0. I have only one
// item in menu, so 0 - "Simplify area".
//

// for debug purposes
//var con = require("josm/scriptingconsole");

var cmd = require("josm/command");
var active_layer = josm.layers.activeLayer;
var ds = active_layer.data;

ds.selection.ways.forEach(function(way, way_id, way_ar) {
    ds.selection.clearAll();
    ds.selection.add(way);
    org.openstreetmap.josm.actions.OrthogonalizeAction().actionPerformed(null);
    josm.menu.get(MORE_TOOLS_INDEX).getItem(SIMPLIFY_AREA_INDEX).doClick(1);
    active_layer.apply(
        cmd.change(way, {tags: {"building" : "yes"}})
    );
});

ds.selection.clearAll();
