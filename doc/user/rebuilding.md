# Rebuilding user documentation
Go back to [README](../../README.md).

This script can be run as follows:

1. Download the script.
2. In JOSM, select raw square buildings.
3. From menu Scripting -> Run.
4. Search for downloaded script.
5. Run it.

Note: The script depends on [Simplify Area plugin][1], it must be installed.

Note: The script is usable only for square buildings.

Note: The [Simplify Area plugin][1] should be installed at the fifth position
in the menu ("File", "Edit", "View", "Tools", "More tools", ...). The *Simplify
Area* menu item should be first item in the *More tools* menu. If not, see
`rebuilding.js` file before run and change `MORE_TOOLS_INDEX` and
`SIMPLIFY_AREA_INDEX` accordingly!

This script folows [data simplification with JOSM][2] by:
1. Orthogonalize each way in selection.
2. Simplify each way in selection (see [Simplify Area plugin][1] for settings
   and more info).
3. Mark each way as `building=yes` (just to be sure).

[1]: https://wiki.openstreetmap.org/wiki/JOSM/Plugins/SimplifyArea
[2]: https://docs.google.com/document/d/1IvGe3a_oS9RjTI8CiryQ39CHKdPyf3lZxayb83QMyJk/edit
